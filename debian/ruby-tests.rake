require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern      = FileList['./spec/**/*_spec.rb'] - FileList['./spec/**/*azure_rm*_spec.rb']
end

task :default => :spec
